/// The player has only 4 guesses and on each incorrect guess the computer will indicate how many
/// letter positions are correct.
/// For example, if the password is MIND and the player guesses MEND, the game will indicate
/// that 3 out of 4 positions are correct (M_ND). If the password is COMPUTE and the player
/// guesses PLAYFUL, the game will report 0/7. While some of the letters match,
/// they're in the wrong position.
/// Ask the player for a difficulty (very easy, easy, average, hard, very hard),
/// then present the player with 5 to 15 words of the same length. The length can be 4 to 15 letters.
/// More words and letters make for a harder puzzle. The player then has 4 guesses,
/// and on each incorrect guess indicate the number of correct positions.
/// Here's an example game:
/// Difficulty (1-5)? 3
/// SCORPION
/// FLOGGING
/// CROPPERS
/// MIGRAINE
/// FOOTNOTE
/// REFINERY
/// VAULTING
/// VICARAGE
/// PROTRACT
/// DESCENTS
/// Guess (4 left)? migraine
/// 0/8 correct
/// Guess (3 left)? protract
/// 2/8 correct
/// Guess (2 left)? croppers
/// 8/8 correct
/// You win!
/// You can draw words from our favorite dictionary file: enable1.txt.
/// Your program should completely ignore case when making the position checks.
/// There may be ways to increase the difficulty of the game,
/// perhaps even making it impossible to guarantee a solution,
/// based on your particular selection of words.
/// For example, your program could supply words that have little letter position overlap so that
/// guesses reveal as little information to the player as possible.
///
/// For more implementation and information check out reddit daily programmer thread:
/// https://www.reddit.com/r/dailyprogrammer/comments/3qjnil/20151028_challenge_238_intermediate_fallout/

extern crate rand;
use std::io;
use rand::{Rng, thread_rng, sample};

fn main() {
    let words_str = include_str!("enable1.txt");
    let words_vec: Vec<&str> = words_str.split("\r\n").collect();
    //Might want to change select_difficulty to return option and treat err instead of -1.
    let difficulty = match select_difficulty(){
        Ok(v) => v,
        Err(e) => {
            println!{"{:?}", e};
            std::process::exit(1);
        }
    };
    let difficulty_f = difficulty as f32;
    let word_length = 4 + (difficulty_f*1.6) as usize;
    println!("Word length is: {}", word_length);
    let hamm_dist = (word_length as f32 * (0.8 - difficulty_f*0.1)) as u8;
    println!("Hamming distance is: {}", hamm_dist);
    let word_count = (15 - difficulty*2) as usize;
    let mut selected_words = select_words(&words_vec, word_length, word_count+1, hamm_dist);
    let hidden_word = selected_words.pop().unwrap();
    for word in selected_words{
        println!("{}", word.to_uppercase());
    }
    guess(&hidden_word);
}

fn hamming_distance(first_str: &str, second_str: &str) -> u8{
    let dist_counter = first_str.chars().zip(second_str.chars()).fold(0,|ctr, (c1,c2)|{
        ctr + (c1==c2) as u8
    });
    dist_counter
}

fn guess(hidden_word: &str){
    for x in 1..5{
        println!("Guess {} left? ", (5-x));
        let mut input = String::new();
        io::stdin().read_line(&mut input).unwrap();
        let guessed_letters = hamming_distance(&input.trim().to_lowercase(), &hidden_word.to_lowercase());
        println!("{}/{} correct",guessed_letters, hidden_word.len());
        if hidden_word.len() == guessed_letters as usize {
            println!("You win!");
            return;
        }
    }
    println!("You lost! The correct word was: {}", hidden_word);
}

fn select_words<'a>(words_vec: &Vec<&'a str>, length: usize, word_count: usize, min_hamming_distance: u8) -> Vec<&'a str> {
    loop {
        let mut matching_words: Vec<&str> = Vec::new();
        let mut words_shuffle = words_vec.clone();
        let mut rng = thread_rng();
        rng.shuffle(words_shuffle.as_mut_slice());
        for word in words_shuffle {
            if word.len() == length {
                match matching_words.iter().next() {
                    Some(&u) => if min_hamming_distance < hamming_distance(u, word) {
                            matching_words.push(word)
                    },
                    None => matching_words.push(word)
                }
            }
        }
        if matching_words.len() >= word_count {
            return sample(&mut rng, matching_words, word_count);
        }
    }
}

fn select_difficulty() -> Result<i32, &'static str> {
    println!("Difficulty (1-5)? ");
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    match input.trim().parse::<i32>() {
        Ok(val @ 1 ... 6) => Ok(val),
        Err(_) | Ok(_) => Err("Difficulty entered is not supported.")
    }
}